package com.mimpi.sab143040106;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    ListView listView;
    String arrMenu[] =
            {"TUGAS BESAR", "1. SAB TUGAS PERTAMA", "2. SAB TUGAS KEDUA", "3. SAB TUGAS KETIGA", "4. SAB TUGAS KEEMPAT", "5. SAB TUGAS KELIMA", "6. SAB TUGAS KEENAM", "7. SAB TUGAS KETUJUH"};
    Button buttonLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogout = (Button) findViewById(R.id.buttonLogout);


        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrMenu)
        );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_LONG).show();
                }  else if (position == 1) {
                    startActivity(new Intent(MainActivity.this, Task1Activity.class));
                } else if (position == 2) {
                    startActivity(new Intent(MainActivity.this, Task2Activity.class));
                } else if (position == 3) {
                    startActivity(new Intent(MainActivity.this, Task3Activity.class));
                } else if (position == 4) {
                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_LONG).show();
                } else if (position == 5) {
                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_LONG).show();
                } else if (position == 6) {
                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_LONG).show();
                } else if (position == 7) {
                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showDialog();
            }
        });

    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Anda Yakin ingin Keluar?");

        // set pesan dari dialog
        alertDialogBuilder
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        // jika tombol diklik, maka akan menutup activity ini
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

}
